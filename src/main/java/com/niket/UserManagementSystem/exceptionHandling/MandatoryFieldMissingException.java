package com.niket.UserManagementSystem.exceptionHandling;

public class MandatoryFieldMissingException extends RuntimeException{

	private static final long serialVersionUID = -470180507998010368L;
	
	public MandatoryFieldMissingException() {
		super();
	}
	
	public MandatoryFieldMissingException(String message) {
		super(message);
	}
	
}
