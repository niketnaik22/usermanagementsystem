package com.niket.UserManagementSystem.exceptionHandling;

public class UserNotFoundException extends RuntimeException{

	private static final long serialVersionUID = -470180507998010368L;
	
	public UserNotFoundException() {
		super();
	}
	
	public UserNotFoundException(final String message) {
		super(message);
	}
	
}
