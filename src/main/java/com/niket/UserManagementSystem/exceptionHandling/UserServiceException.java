package com.niket.UserManagementSystem.exceptionHandling;

public class UserServiceException extends RuntimeException{

	public UserServiceException() {
		super();
	}
	public UserServiceException(final String message) {
		super(message);
	}
}
