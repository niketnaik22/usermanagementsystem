package com.niket.UserManagementSystem.exceptionHandling;

public class ExceptionResponse {
	private String message;
	private String requestedURI;
	private boolean success = false;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(final String errorMessage) {
		this.message = errorMessage;
	}

	public String getRequestedURI() {
		return requestedURI;
	}

	public void callerURL(final String requestedURI) {
		this.requestedURI = requestedURI;
	}

	public boolean getSuccess() {
		return success;
	}
}
