package com.niket.UserManagementSystem.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.niket.UserManagementSystem.entity.User;
import com.niket.UserManagementSystem.exceptionHandling.MandatoryFieldMissingException;
import com.niket.UserManagementSystem.exceptionHandling.UserNotFoundException;
import com.niket.UserManagementSystem.exceptionHandling.UserServiceException;
import com.niket.UserManagementSystem.repository.UserRepository;
import com.niket.UserManagementSystem.service.UserService;

/*
 * Service class for all usermanagement related operations
 * 
 */

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepo; 
	
	/* Get AllUsers from tables */
	@Override
	public List<User> getAllUsers() throws UserServiceException, UserNotFoundException{
		List<User> list = null;
		try {
			list = userRepo.findAllUsers(); 
			if(list == null || list.size() == 0) {
				throw new UserNotFoundException();
			}
		}
		catch(UserNotFoundException e) {
			throw new UserNotFoundException("No User found");
		}
		catch (Exception e) {
			throw new UserServiceException("500, Internal server issue");
		}
		
		return list;
	}

	/* Get AllUsers from tables by user Id */
	@Override
	public User getUserById(Long id) throws UserServiceException, UserNotFoundException{
		User user = null;
		try {
			Optional<User> tempUser = userRepo.findUserById(id);
			if(tempUser.isPresent()) {
				user = tempUser.get();
			}else {
				System.out.println("user not present");
				throw new UserNotFoundException("User not found");
			}
		}
		catch(UserNotFoundException e) {
			throw new UserNotFoundException("No User found");
		}
		catch(Exception e) {
			throw new UserServiceException("500, Internal server issue");
		}
		return user;
	}

	/* add user into tables */
	@Override
	public boolean createUser(User user) throws UserServiceException, UserNotFoundException{
		boolean result = false;
		try {
			// add validations for firstName and lastName mandatory
			if(user.getFirstName() == null || user.getFirstName().equalsIgnoreCase("") || user.getLastName() == null || user.getLastName().equalsIgnoreCase("")) {
				throw new MandatoryFieldMissingException("Mandatory Field Missing");
			}else {
				User savedUser = userRepo.save(user);
				result = true;
			}
			
		}
		catch(MandatoryFieldMissingException e) {
			throw new MandatoryFieldMissingException(e.getMessage());
		}
		catch(Exception e) {
			throw new UserServiceException("500, Internal server issue");
		}
		
		return result;
	}

	/* Delete user by Id*/
	@Override
	public boolean deleteUser(Long id) {
		boolean result = false;
		try {
			User user = getUserById(id);
			user.setIsDeleted(1);
			userRepo.save(user);
			result = true;
		}
		catch(UserNotFoundException e) {
			throw new UserNotFoundException("No User found");
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new UserServiceException("500, Internal server issue");
		}
		return result;
	}

	/*
	 * API to search user by serach keyword
	 */
	@Override
	public List<User> searchUserByKeyword(String keyword) throws UserServiceException, UserNotFoundException{
		List<User> list = null;
		try {
			list = userRepo.searchUserByKeyword(keyword);
			if(list == null || list.size()==0) {
				throw new UserNotFoundException();
			}
		}catch(UserNotFoundException e) {
			throw new UserNotFoundException("No User found");
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new UserServiceException("500, Internal server issue");
		}
		return list;
	}
	
	
	
	
	
	
	
}
