package com.niket.UserManagementSystem.service;

import java.util.List;

import com.niket.UserManagementSystem.entity.User;

public interface UserService {
	
	public List<User> getAllUsers();

	public User getUserById(Long id);

	public boolean createUser(User user);

	public boolean deleteUser(Long id);

	public List<User> searchUserByKeyword(String keyword);
	
	
}
