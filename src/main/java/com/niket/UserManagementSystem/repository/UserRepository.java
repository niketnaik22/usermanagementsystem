package com.niket.UserManagementSystem.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.niket.UserManagementSystem.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query(value = "FROM User WHERE isDeleted <> 1")
	List<User> findAllUsers();
	
	@Query(value = "FROM User WHERE id=:id and isDeleted <> 1")
	Optional<User> findUserById(@Param("id") Long id);
	
	@Query(value = "SELECT * FROM User user WHERE  CONCAT(LOWER(user.first_name), ' ', LOWER(user.last_name)) like CONCAT('%',LOWER(:keyword) ,'%')", nativeQuery = true)
	List<User> searchUserByKeyword(@Param("keyword") String keyword);

	
}
