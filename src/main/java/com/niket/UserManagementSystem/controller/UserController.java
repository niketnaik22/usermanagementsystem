package com.niket.UserManagementSystem.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.niket.UserManagementSystem.entity.User;
import com.niket.UserManagementSystem.response.ApiResponse;
import com.niket.UserManagementSystem.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	/* API to get All users */
	@GetMapping("/getAllUsers")
	public ApiResponse getAllUsers(){
		List<User> list = userService.getAllUsers();
		return new ApiResponse(true, "SUCCESS", list);
	}
	
	/* API to get user by id */
	@GetMapping("/getUser/{id}")
	public ApiResponse getUser(@PathVariable Long id) {
		User user = userService.getUserById(id);
		return new ApiResponse(true, "SUCCESS", user);
	}
	
	/* API to create User */
	@PostMapping("/createUser")
	public ApiResponse createUser(@RequestBody User user) {
		boolean result = userService.createUser(user);
		return result ? new ApiResponse(true, "SUCCESS", "User Created Successfully") : new ApiResponse(true, "FAILD") ;
	}
	
	/* API to delete user by id */
	@PostMapping("/deleteUser")
	public ApiResponse deleteUser(@RequestBody User user) {
		boolean result = userService.deleteUser(user.getId());
		return result ? new ApiResponse(true, "SUCCESS", "User Deleted") : new ApiResponse(true, "FAILD") ;
	}
	
	/* API to search users by keyword */
	@PostMapping("/searchUserByName")
	public ApiResponse searchUserByName(@RequestBody Map<String, Object> reqData) {
		List<User> list = null;
		String keyword = (String)reqData.get("keyword");
		list = userService.searchUserByKeyword(keyword);
				
		return new ApiResponse(true, "SUCCESS", list);
	}
	
	
}
