package com.niket.UserManagementSystem.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name="user")
@Data
public class User {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;
	
    @Column
    private Date dob;
    
    @Column 
    private String city;
    
    @Column
    private String mobile;
    
    @Column
    @JsonIgnore
    private int isDeleted;
    
}
